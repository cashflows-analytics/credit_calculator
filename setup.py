from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Tool to automate assessment of merchant credit worthiness',
    author='Jonathan Fletcher',
    license='MIT',
)
